import axios from 'axios'
import VueCookies from 'vue-cookies'

const state = {
  userToken: null,
}

const getters = {
  isAuthenticated: state => !!state.userToken,
  StateUser: state => state.userToken
}

const actions = {
  async LogIn ({commit}, User) {
    let response = await axios.post('api/authentication_token', User, {
      headers: { 'Content-Type': 'application/json' }
    })
    VueCookies.config('3h')
    VueCookies.set('tokenUser', response.data.token)
    await commit('setUser', response.data.token)
  },
  async LogOut ({commit}) {
    let user = null
    commit('LogOut', user)
  }

}

const mutations = {
  setUser (state, username) {
    state.userToken = username
  },
  LogOut (state) {
    state.userToken = null
  }
}
export default {
  state,
  getters,
  actions,
  mutations
}
