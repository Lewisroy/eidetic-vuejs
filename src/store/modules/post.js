import axios from 'axios'
import VueCookies from 'vue-cookies'

const state = {
  post: null
}

const getters = {
  StatePosts: state => state.posts
}

const actions = {
  async CreatePost ({dispatch}, post) {
    let token = VueCookies.get('tokenUser')
    await axios.post('api/eidetics', post, {
      headers: { 'Content-Type': 'application/ld+json', 'Authorization': 'Bearer ' + token }
    }).catch(function (error) {
      if (error.response) {
        if (error.response.status === 401) {
          VueCookies.remove('tokenUser')
        }
      }
    })
    await dispatch('GetPosts')
  },
  async GetPosts ({ commit }) {
    let token = VueCookies.get('tokenUser')
    let response = await axios.get('api/eidetics/', {
      headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token }
    }).catch(function (error) {
      if (error.response) {
        if (error.response.status === 401) {
          VueCookies.remove('tokenUser')
          commit('LogOut', null)
        }
      }
    })
    commit('setPosts', response.data['hydra:member'])
  }
}

const mutations = {
  setPosts (state, posts) {
    state.posts = posts
  }
}
export default {
  state,
  getters,
  actions,
  mutations
}
