# eidetic-vuejs

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Cordova commands

``` bash
# Init cordova
npm run cordova prepare
```

### Compile on android

``` bash
# Dev mode
npm run cordova-serve-android

# Production mode
npm run cordova-build-android
```

### Compile on IOS

``` bash
# Dev mode
npm run cordova-serve-ios

# Production mode
npm run cordova-build-ios
```